# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.template.defaultfilters import slugify
import django.utils
from django.contrib.postgres.fields import JSONField


# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=128, unique=True, blank=False, default=None)
    slug = models.SlugField(unique=True, default=slugify(name))
    created = models.DateField(default=django.utils.timezone.now)
    tooltip = models.CharField(max_length=128)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Workflow(models.Model):
    name = models.CharField(max_length=128, unique=True, blank=False, default="")
    slug = models.SlugField(unique=True, default=slugify(name))
    description = models.CharField(max_length=512, default="")
    views = models.IntegerField(default=0)
    downloads = models.IntegerField(default=0)
    versionInit = models.CharField(max_length=128, default="")
    category = models.ManyToManyField(Category)
    client_ip = models.GenericIPAddressField(default="192.0.0.0")
    keywords = models.CharField(max_length=128, default="")
    json = models.CharField(default = "", max_length=5000)
    created = models.DateField(default=django.utils.timezone.now)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Workflow, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name
