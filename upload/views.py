# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from forms import WorkflowFormBase

def add_workflow(request):
    form = WorkflowFormBase()
    msg = ""
    # A HTTP POST?
    if request.method == 'POST':
        form = WorkflowFormBase(request.POST, request.FILES)

        # Have we been provided with a valid form?
        if form.is_valid():
            workflowFile = form.cleaned_data['json']
            file_data = workflowFile.read().decode('utf-8')
            form.instance.json = file_data
            form.save(commit=True)
            msg = "El workflow se ha introducido correctamente"
        else:
            print(form.errors)

    return render(request, 'add_workflow.html', {'form': form, 'msg': msg})