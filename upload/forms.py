from django import forms
from data.models import Workflow, Category


class WorkflowFormBase(forms.ModelForm):

    name = forms.CharField(max_length=128,
        help_text="Introduzca nombre del workflow:")
    category = forms.ModelMultipleChoiceField(queryset=Category.objects.all(), help_text="Seleccione la categoria del workflow: ")
    keywords = forms.CharField(initial=0, help_text="Introduzca keywords del workflow: ")
    description = forms.CharField(initial=0, help_text="Introduzca descripcion del workflow: ")
    versionInit = forms.CharField(initial=0, help_text="Introduzca version del workflow: ")
    json = forms.FileField(help_text="Fichero para el workflow: ")

    #An inline class to provide additional information on the form.
    class Meta:
        #Provide an association between the ModelForm and a model
        model = Workflow
        fields = ('name', 'category', 'keywords', 'description', 'versionInit', 'json',)