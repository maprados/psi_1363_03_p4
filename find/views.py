# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from data.models import Category, Workflow
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
import json

# Create your views here.


def workflow_list(request, category_slug=None):

    # YOUR CODE GOES HERE
    # queries that fill, category, categories, workflows
    # and error

    if category_slug == None:
        category = None
    else:
        category = Category.objects.get(slug=category_slug)

    categories = Category.objects.all()

    found = True

    if category_slug == None:
        workflows = Workflow.objects.all()
    else:
        try:
            workflows = Workflow.objects.filter(category=category)
        except ObjectDoesNotExist:
            found = False
            workflows = None

    error = "No existen workflows asignadas a esta categoria"

    aux = {'category': category,  # category associated to category_slug
    'categories': categories,  # list with all categories
    'workflows': workflows,    # all workflows associated to category
    'result': found,           # False if no workflow satisfices the query
    'error': error             # message to display if results == False
    }

    return render(request, 'list.html', aux)



def workflow_detail(request, id, slug):
    #Your code goes here
    #query that returns the workflow with id=id

    result = True
    error = "No existe un workflow con el id especificado"

    try:
        workflow = Workflow.objects.get(id=id)
    except ObjectDoesNotExist:
        result = False
        workflow = None

    dict = {}
    dict['result'] = result      # False if no workflow satisfices the query
    dict['workflow'] = workflow  # workflow with id = id
    dict['error'] = error  # message to display if results == False

    return render(request, 'detail.html', dict)


def workflow_search(request):
    #YOUR CODE GOES HERE
    #query that returns the workflow with name = name

    result = True
    error = "No existe un workflow con el nombre especificado"

    try:
        workflow = Workflow.objects.get(name=request.POST.get('key'))
    except ObjectDoesNotExist:
        result = False
        workflow = None

    dict = {}
    dict['result'] = result      # False if no workflow satisfices the query
    dict['workflow'] = workflow  # workflow with name = name
    dict['error'] = error    # message to display if results == False

    return render(request, 'detail.html', dict)


def workflow_download(request, id, slug, count = True):

    try:
        workflow = Workflow.objects.get(id=id)
    except ObjectDoesNotExist:
        workflow = None
        error = "No se ha podido descargar el workflow"
        return render(request, 'detail.html', workflow, error)

    if count is True:
        workflow.downloads += 1
        workflow.views += 1
        workflow.save()

    response = HttpResponse(workflow.json, content_type="application/octet-stream")

    filename = 'Fichero_%s' % slug
    response['Content-Disposition'] = 'inline; filename=%s' %filename

    return response


def workflow_download_json(request, id, slug):

    try:
        workflow = Workflow.objects.get(id=id)
    except ObjectDoesNotExist:
        workflow = None
        error = "No se ha podido descargar el workflow"
        return render(request, 'detail.html', workflow, error)

    return HttpResponse(workflow.json, content_type="application/octet-stream")
